FROM openjdk:16-jdk-oraclelinux8
ADD target/dxc-0.0.1-SNAPSHOT.jar thyme.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","thyme.jar"]