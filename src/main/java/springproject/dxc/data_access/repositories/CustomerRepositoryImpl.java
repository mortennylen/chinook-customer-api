package springproject.dxc.data_access.repositories;

import org.springframework.stereotype.Repository;
import springproject.dxc.data_access.utils.ConnectionHelper;
import springproject.dxc.data_access.interfaces.CustomerRepository;
import springproject.dxc.models.Customer;
import springproject.dxc.models.CustomersByCountry;
import springproject.dxc.models.FavouriteGenre;
import springproject.dxc.models.Spender;
import java.sql.*;
import java.util.LinkedList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Repository
public class CustomerRepositoryImpl implements CustomerRepository {

    // Setting up the connection object we need.
    private String URL = ConnectionHelper.CONNECTION_URL;
    private Connection conn = null;

    Logger logger = LoggerFactory.getLogger(CustomerRepositoryImpl.class);

    @Override
    public LinkedList<Customer> getAllCustomers() {

        LinkedList<Customer> customerList = new LinkedList<>();

        try {
            //Connect to the database with JDBC.
            conn = DriverManager.getConnection(URL);
            logger.info("Connection established.");

            //Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT CustomerID, FirstName, LastName, Country, PostalCode, Phone, Email " +
                            "FROM Customer");

            // Execute query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customerList.add(new Customer(
                        resultSet.getInt("CustomerID"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("Lastname"),
                        resultSet.getString("Country"),
                        resultSet.getString("PostalCode"),
                        resultSet.getString("Phone"),
                        resultSet.getString("Email")
                ));
            }
            logger.info("Run getAllCustomers successfully.");

        } catch (SQLException e) {
            logger.error(e.getMessage());
        } finally {
            try {
                conn.close();
                logger.info("Connection closed successfully.");
            } catch (Exception e) {
                logger.error(e.getMessage());
            }
        }

        return customerList;
    }

    @Override
    public Customer getCustomerById(int customerId) {
        Customer customer = null;

        try {
            // Connect to the database with JDBC.
            conn = DriverManager.getConnection(URL);
            logger.info("Connection established.");


            //Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT CustomerID, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer as C WHERE C.CustomerID=?");


            preparedStatement.setString(1, Integer.toString(customerId));

            //Execute query
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            customer = new Customer(
                    resultSet.getInt("CustomerID"),
                    resultSet.getString("FirstName"),
                    resultSet.getString("Lastname"),
                    resultSet.getString("Country"),
                    resultSet.getString("PostalCode"),
                    resultSet.getString("Phone"),
                    resultSet.getString("Email")
            );

            logger.info("Run getCustomerById successfully.");

        } catch (SQLException e) {
            logger.error(e.getMessage());
        } finally {
            try {
                conn.close();
                logger.info("Connection closed successfully.");
            } catch (Exception e) {
                logger.error(e.getMessage());
            }
        }

        return customer;
    }

    @Override
    public LinkedList<Customer> getCustomerByName(String name) {
        LinkedList<Customer> customerList = new LinkedList<>();

        try {
            // Connect to the database with JDBC.
            conn = DriverManager.getConnection(URL);
            logger.info("Connection established.");

            //Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT CustomerID, FirstName, LastName, Country, PostalCode, Phone, Email\n" +
                            "FROM Customer\n" +
                            "WHERE FirstName LIKE ?\n" +
                            "   OR LastName LIKE ?");


            String preparedString = "%" + name + "%";

            preparedStatement.setString(1, preparedString);
            preparedStatement.setString(2, preparedString);

            //Execute query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customerList.add(new Customer(
                        resultSet.getInt("CustomerID"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("Lastname"),
                        resultSet.getString("Country"),
                        resultSet.getString("PostalCode"),
                        resultSet.getString("Phone"),
                        resultSet.getString("Email")
                ));
                logger.info("Run getCustomerByName successfully.");
            }

        } catch (SQLException e) {
            logger.error(e.getMessage());
        } finally {
            try {
                conn.close();
                logger.info("Connection closed successfully.");
            } catch (Exception e) {
                logger.error(e.getMessage());
            }
        }
        return customerList;
    }

    @Override
    public LinkedList<Customer> getPageOfCustomers(int limit, int offset) {
        LinkedList<Customer> customerList = new LinkedList<>();

        try {
            //Connect to the database with JDBC.
            conn = DriverManager.getConnection(URL);
            logger.info("Connection established.");

            //Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT CustomerID, FirstName, LastName, Country, PostalCode, Phone, Email \n" +
                            "FROM Customer \n" +
                            "LIMIT ? OFFSET ?;");

            preparedStatement.setString(1, Integer.toString(limit));
            preparedStatement.setString(2, Integer.toString(offset));

            // Execute query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customerList.add(new Customer(
                        resultSet.getInt("CustomerID"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("Lastname"),
                        resultSet.getString("Country"),
                        resultSet.getString("PostalCode"),
                        resultSet.getString("Phone"),
                        resultSet.getString("Email")
                ));
            }
            logger.info("Run searchTracksByName successfully.");

        } catch (SQLException e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        } finally {
            try {
                conn.close();
                logger.info("Connection closed successfully.");
            } catch (Exception e) {
                e.printStackTrace();
                logger.error(e.getMessage());
            }
        }

        return customerList;
    }

    @Override
    public boolean addCustomer(Customer customer) {

        try {
            //Connect to the database with JDBC.
            conn = DriverManager.getConnection(URL);
            logger.info("Connection established.");

            //Make SQL Query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("INSERT INTO Customer (FirstName, LastName, Country, PostalCode, Phone, Email)\n" +
                            "VALUES (?,?,?,?,?,?)");

            preparedStatement.setString(1, customer.getFirstName());
            preparedStatement.setString(2, customer.getLastName());
            preparedStatement.setString(3, customer.getCountry());
            preparedStatement.setString(4, customer.getPostalCode());
            preparedStatement.setString(5, customer.getPhone());
            preparedStatement.setString(6, customer.getEmail());

            //Execute query
            preparedStatement.executeUpdate();

            logger.info("Run addCustomer successfully.");
        } catch (SQLException e) {
            logger.error(e.getMessage());
            return false;
        } finally {
            try {
                conn.close();
                logger.info("Connection closed successfully.");
            } catch (Exception e) {
                logger.error(e.getMessage());
                return false;
            }
        }
        return true;
    }


    @Override
    public boolean updateCustomer(Customer customer) {

        try {
            //Connect to the database with JDBC.
            conn = DriverManager.getConnection(URL);
            logger.info("Connection established.");


            //Make SQL Query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("UPDATE Customer\n" +
                            "SET FirstName = coalesce(?, FirstName),\n" +
                            "    LastName  = coalesce(?, LastName),\n" +
                            "    Country=coalesce(?, Country),\n" +
                            "    PostalCode=coalesce(?, PostalCode),\n" +
                            "    Phone=coalesce(?, Phone),\n" +
                            "    Email=coalesce(?, Email)\n" +
                            "WHERE CustomerID = ?;");

            preparedStatement.setString(1, customer.getFirstName());
            preparedStatement.setString(2, customer.getLastName());
            preparedStatement.setString(3, customer.getCountry());
            preparedStatement.setString(4, customer.getPostalCode());
            preparedStatement.setString(5, customer.getPhone());
            preparedStatement.setString(6, customer.getEmail());
            preparedStatement.setInt(7, customer.getCustomerID());

            //Execute query
            preparedStatement.executeUpdate();

            logger.info("Run updateCustomer successfully.");
        } catch (SQLException e) {
            logger.error(e.getMessage());
        } finally {
            try {
                conn.close();
                logger.info("Connection closed successfully.");
            } catch (Exception e) {
                logger.error(e.getMessage());
            }
        }

        return true;
    }


    @Override
    public LinkedList<CustomersByCountry> getNumberOfCustomersByCountry() {
        LinkedList<CustomersByCountry> customersByCountry = new LinkedList<>();

        try {
            //Connect to the database with JDBC.
            conn = DriverManager.getConnection(URL);
            logger.info("Connection established.");

            //Make SQL Query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT Country, COUNT(CustomerId) AS Customers " +
                            "FROM Customer " +
                            "GROUP BY Country " +
                            "ORDER BY COUNT(CustomerId) DESC");

            //Execute query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customersByCountry.add(new CustomersByCountry(
                        resultSet.getString("Country"),
                        resultSet.getInt("Customers")));
            }
            logger.info("Run getNumberOfCustomersByCountry successfully.");

        } catch (SQLException e) {
            logger.error(e.getMessage());
        } finally {
            try {
                conn.close();
                logger.info("Connection closed successfully.");
            } catch (Exception e) {
                logger.error(e.getMessage());
            }
        }

        return customersByCountry;
    }

    @Override
    public LinkedList<Spender> getHighestSpenders() {
        LinkedList<Spender> spenderList = new LinkedList<>();

        try {
            //Connect to the database with JDBC.
            conn = DriverManager.getConnection(URL);
            logger.info("Connection established.");

            //Make SQL Query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT Customer.CustomerId, FirstName, LastName, SUM(Total) AS Total\n" +
                            "FROM Customer JOIN Invoice\n" +
                            "ON Customer.CustomerId = Invoice.CustomerId\n" +
                            "GROUP BY Customer.CustomerId\n" +
                            "ORDER BY SUM(Total) DESC;");

            //Execute query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                spenderList.add(new Spender(
                        resultSet.getInt("CustomerID"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("Lastname"),
                        resultSet.getDouble("Total")

                ));
                logger.info("Run getHighestSpenders successfully.");
            }

        } catch (SQLException e) {
            logger.error(e.getMessage());
        } finally {
            try {
                conn.close();
                logger.info("Connection closed successfully.");
            } catch (Exception e) {
                logger.error(e.getMessage());
            }
        }

        return spenderList;
    }


    @Override
    public LinkedList<FavouriteGenre> getMostPopularGenreByCustomerID(int customerID) {
        LinkedList<FavouriteGenre> favouriteGenreList = new LinkedList<>();

        try {
            //Connect to the database with JDBC.
            conn = DriverManager.getConnection(URL);
            logger.info("Connection established.");

            //Make SQL Query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("WITH X AS (\n" +
                            "    SELECT Customer.FirstName, Genre.name, COUNT(Genre.GenreId) AS GenreCount\n" +
                            "    FROM Customer\n" +
                            "    INNER JOIN Invoice ON Customer.CustomerId = Invoice.CustomerId\n" +
                            "    INNER JOIN InvoiceLine ON Invoice.InvoiceId = InvoiceLine.InvoiceId\n" +
                            "    INNER JOIN Track ON InvoiceLine.TrackId = Track.TrackId\n" +
                            "    INNER JOIN Genre ON Track.GenreId = Genre.GenreId\n" +
                            "    WHERE Invoice.CustomerId == ?\n" +
                            "    GROUP BY Genre.name)\n" +
                            "SELECT name AS Genre, GenreCount\n" +
                            "FROM X\n" +
                            "WHERE (SELECT MAX(GenreCount) FROM X) = GenreCount;");

            preparedStatement.setString(1, Integer.toString(customerID));

            //Execute query
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                favouriteGenreList.add(new FavouriteGenre(
                        resultSet.getString("Genre"),
                        resultSet.getInt("GenreCount")
                ));
            }
            logger.info("Run getMostPopularGenreByCustomerID successfully.");

        } catch (SQLException e) {
            logger.error(e.getMessage());
        } finally {
            try {
                conn.close();
                logger.info("Connection closed successfully.");
            } catch (Exception e) {
                logger.error(e.getMessage());
            }
        }

        return favouriteGenreList;
    }

}
