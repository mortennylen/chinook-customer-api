package springproject.dxc.data_access.repositories;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import springproject.dxc.data_access.interfaces.ArtistRepository;
import springproject.dxc.data_access.utils.ConnectionHelper;
import springproject.dxc.models.Artists;
import java.sql.*;
import java.util.LinkedList;

@Repository
public class ArtistRepositoryImpl implements ArtistRepository {
    private String URL = ConnectionHelper.CONNECTION_URL;
    private Connection conn = null;

    Logger logger = LoggerFactory.getLogger(ArtistRepositoryImpl.class);

    @Override
    public LinkedList<Artists> getRandomArtists() {

        LinkedList<Artists> genreLinkedList = new LinkedList<>();

        try {
            //Connect to the database with JDBC.
            conn = DriverManager.getConnection(URL);
            logger.info("Connection established.");

            //Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT Name FROM Artist\n" +
                            "ORDER BY random ()\n" +
                            "LIMIT 5;");

            // Execute query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                genreLinkedList.add(new Artists(resultSet.getString("Name")));
            }
            logger.info("Run getRandomArtists successfully.");

        } catch (SQLException e) {
            logger.error(e.getMessage());
        } finally {
            try {
                conn.close();
                logger.info("Connection closed successfully.");
            } catch (Exception e) {
                logger.error(e.getMessage());
            }
        }

        return genreLinkedList;
    }
}
