package springproject.dxc.data_access.repositories;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import springproject.dxc.data_access.utils.ConnectionHelper;
import springproject.dxc.data_access.interfaces.GenreRepository;
import springproject.dxc.models.Genre;
import java.sql.*;
import java.util.LinkedList;

@Repository
public class GenreRepositoryImpl implements GenreRepository {
    private String URL = ConnectionHelper.CONNECTION_URL;
    private Connection conn = null;

    Logger logger = LoggerFactory.getLogger(GenreRepositoryImpl.class);

    @Override
    public LinkedList<Genre> getRandomGenres() {

        LinkedList<Genre> genreLinkedList = new LinkedList<>();

        try {
            //Connect to the database with JDBC.
            conn = DriverManager.getConnection(URL);
            logger.info("Connection established.");

            //Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT Name FROM Genre\n" +
                            "ORDER BY random ()\n" +
                            "LIMIT 5;");

            // Execute query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                genreLinkedList.add(new Genre(resultSet.getString("Name")));
            }

            logger.info("Run getRandomGenres successfully.");
        } catch (SQLException e) {
            logger.error(e.getMessage());
        } finally {
            try {
                conn.close();
                logger.info("Connection closed successfully.");
            } catch (Exception e) {
                logger.error(e.getMessage());
            }
        }

        return genreLinkedList;
    }
}
