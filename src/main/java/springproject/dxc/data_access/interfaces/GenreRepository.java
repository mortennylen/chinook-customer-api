package springproject.dxc.data_access.interfaces;

import springproject.dxc.models.Genre;

import java.util.LinkedList;

public interface GenreRepository {
    public LinkedList<Genre> getRandomGenres();
}
