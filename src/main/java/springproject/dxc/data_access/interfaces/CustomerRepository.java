package springproject.dxc.data_access.interfaces;

import springproject.dxc.models.Customer;
import springproject.dxc.models.CustomersByCountry;
import springproject.dxc.models.FavouriteGenre;
import springproject.dxc.models.Spender;

import java.util.LinkedList;



public interface CustomerRepository {
    public LinkedList<Customer> getAllCustomers();
    public Customer getCustomerById(int customerId);
    public LinkedList<Customer> getCustomerByName(String name);
    public LinkedList<Customer> getPageOfCustomers(int limit, int offset);
    public boolean addCustomer(Customer customer);
    public boolean updateCustomer(Customer customer);
    public LinkedList<CustomersByCountry> getNumberOfCustomersByCountry(); //descending order
    public LinkedList<Spender> getHighestSpenders(); //descending order
    public LinkedList<FavouriteGenre> getMostPopularGenreByCustomerID(int id);
}
