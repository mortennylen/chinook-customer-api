package springproject.dxc.data_access.interfaces;

import springproject.dxc.models.Track;

import java.util.LinkedList;

public interface TrackRepository {
    public LinkedList<Track> get5Tracks();
    public LinkedList<Track> searchTracksByName(String title);


}
