package springproject.dxc.data_access.interfaces;

import springproject.dxc.models.Artists;

import java.util.LinkedList;

public interface ArtistRepository {
    public LinkedList<Artists> getRandomArtists();

}
