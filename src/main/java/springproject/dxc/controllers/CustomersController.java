package springproject.dxc.controllers;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.web.bind.annotation.*;

import springproject.dxc.models.Customer;
import springproject.dxc.models.CustomersByCountry;
import springproject.dxc.models.FavouriteGenre;
import springproject.dxc.models.Spender;
import springproject.dxc.data_access.interfaces.CustomerRepository;

import java.util.LinkedList;

@RestController
public class CustomersController {

    @Autowired
    CustomerRepository customerRepository;

    @GetMapping("/api/customer")
    public LinkedList<Customer> getAllCustomers() {
        return customerRepository.getAllCustomers();
    }

    @PatchMapping(value = "/api/customer", consumes = "application/json", produces = "application/json")
    public boolean updateCustomer(@RequestBody Customer customer) {
        return customerRepository.updateCustomer(customer);
    }

    @PostMapping(value = "/api/customer", consumes = "application/json", produces = "application/json")
    public boolean addCustomer(@RequestBody Customer customer) {
        return customerRepository.addCustomer(customer);
    }

    @GetMapping("/api/customer/{id}")
    public Customer getCustomerById(@PathVariable int id) {
        return customerRepository.getCustomerById(id);
    }

    @GetMapping("/api/customer/by-page")
    public LinkedList<Customer> getPageOfCustomers(@RequestParam int limit, @RequestParam int offset) {
        return customerRepository.getPageOfCustomers(limit, offset);
    }

    @GetMapping("/api/customer/by-name")
    public LinkedList<Customer> getCustomerByName(@RequestParam String search) {
        return customerRepository.getCustomerByName(search);
    }

    @GetMapping("/api/customer/by-country")
    public LinkedList<CustomersByCountry> getNumberOfCustomersByCountry() {
        return customerRepository.getNumberOfCustomersByCountry();
    }

    @GetMapping("/api/customer/highest-spenders")
    public LinkedList<Spender> getHighestSpenders() {
        return customerRepository.getHighestSpenders();
    }

    @GetMapping("/api/customer/{id}/favourite-genre")
    public LinkedList<FavouriteGenre> getMostPopularGenreByCustomerID(@PathVariable int id) {
        return customerRepository.getMostPopularGenreByCustomerID(id);
    }

}
