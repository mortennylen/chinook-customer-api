package springproject.dxc.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import springproject.dxc.data_access.repositories.*;
import springproject.dxc.data_access.interfaces.ArtistRepository;
import springproject.dxc.data_access.interfaces.GenreRepository;
import springproject.dxc.data_access.interfaces.TrackRepository;

@Controller
public class ThymeleafController {

    ArtistRepository artistRepository = new ArtistRepositoryImpl();
    TrackRepository trackRepository = new TrackRepositoryImpl();
    GenreRepository genreRepository = new GenreRepositoryImpl();

    @GetMapping("/")
    public String index(Model model) {
        model.addAttribute("artists",artistRepository.getRandomArtists());
        model.addAttribute("tracks",trackRepository.get5Tracks());
        model.addAttribute("genres",genreRepository.getRandomGenres());
        return "index";
    }



   @PostMapping("/search")
   public String index(@RequestParam("searchTerm") String searchTerm, Model model) {
        model.addAttribute("tracks",trackRepository.searchTracksByName(searchTerm));
        return "search";
    }


}
