package springproject.dxc.models;

public class Spender {
    private int customerID;
    private String firstName;
    private String lastName;
    private double totalAmountSpend;

    public Spender(int customerID, String firstName, String lastName, double totalAmountSpend) {
        this.customerID = customerID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.totalAmountSpend = totalAmountSpend;
    }

    public int getCustomerID() {
        return customerID;
    }

    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public double getTotalAmountSpend() {
        return totalAmountSpend;
    }

    public void setTotalAmountSpend(double totalAmountSpend) {
        this.totalAmountSpend = totalAmountSpend;
    }
}
