package springproject.dxc.models;

public class FavouriteGenre {
    private String favouriteGenre;
    private int genreCount;

    public String getFavouriteGenre() {
        return favouriteGenre;
    }

    public void setFavouriteGenre(String favouriteGenre) {
        this.favouriteGenre = favouriteGenre;
    }

    public int getGenreCount() {
        return genreCount;
    }

    public void setGenreCount(int genreCount) {
        this.genreCount = genreCount;
    }

    public FavouriteGenre(String favouriteGenre, int genreCount) {
        this.favouriteGenre = favouriteGenre;
        this.genreCount = genreCount;

    }
}



